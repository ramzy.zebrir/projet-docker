<?php
    ini_set('display_errors', 'off'); // La méthode crypt renvoie des warnings malgrè son bon fonctionnement
    session_start();
    include('connexionDB.php'); // Fichier PHP contenant la connexion à votre BDD
 
  // S'il y a une session alors on ne retourne plus sur cette page  
    if (isset($_SESSION['id'])){
        header('Location: index.php');
        exit;
    }
 
    // Si la variable "$_Post" contient des informations alors on les traitres
    if(!empty($_POST)){
        extract($_POST);
        $valid = true;
 
        if (isset($_POST['connexion'])){
            $mail = htmlentities(strtolower(trim($mail)));
            $mdp = trim($mdp);
 
            if(empty($mail)){ // Vérification qu'il y est bien un mail de renseigné
                $valid = false;
                $er_mail = "Il faut mettre un mail";
            }
 
            if(empty($mdp)){ // Vérification qu'il y est bien un mot de passe de renseigné
                $valid = false;
                $er_mdp = "Il faut mettre un mot de passe";
            }
 
            // On fait une requête pour savoir si le couple mail / mot de passe existe bien car le mail est unique !
            $req = $DB->query("SELECT * 
                FROM users 
                WHERE mail = ? AND mdp = ?",
                array($mail, crypt($mdp, "$5$rounds=5000$usesomesillystringforsalt$")));
            $req = $req->fetch();
 
            // Si on a pas de résultat alors c'est qu'il n'y a pas d'utilisateur correspondant au couple mail / mot de passe
            if ($req['id'] == ""){
                $valid = false;
                $er_mail = "Le mail ou le mot de passe est incorrecte";
            }
 
            // S'il y a un résultat alors on va charger la SESSION de l'utilisateur en utilisateur les variables $_SESSION
            if ($valid){
                $_SESSION['id'] = $req['id']; // id de l'utilisateur unique pour les requêtes futures
                $_SESSION['nom'] = $req['nom'];
                $_SESSION['prenom'] = $req['prenom'];
                $_SESSION['mail'] = $req['mail'];
                header('Location:  index.php');
                exit;
            }   
        }
    }

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Connexion</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    </head>
<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php">Accueil</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
                <?php
                   if(!isset($_SESSION['id'])){
                      // ... 
                   }else{
                   ?>
                      <li class="nav-item">
                            <a class="nav-link" href="profil.php">Mon profil</a>
                      </li>
                   <?php
                   } 
                ?>
          </ul>
          <ul class="navbar-nav ml-md-auto">
                <?php
                   if(!isset($_SESSION['id'])){
                   ?>
                      <li class="nav-item">
                            <a class="nav-link" href="inscription.php">Inscription</a>
                      </li>
                      <li class="nav-item">
                            <a class="nav-link" href="connexion.php">Connexion</a>
                      </li>
                   <?php
                   }else{
                   ?>
                      <li class="nav-item">
                            <a class="nav-link" href="deconnexion.php">Déconnexion</a>
                      </li>   
                    <?php
                   }
                ?>                  
          </ul>
       </div>
    </nav>
    
    <div class="container">
        <div class="row">   
            <div class="col-0 col-sm-0 col-md-2 col-lg-3"></div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-6">
        <h1>Connexion</h1>
        <form method="post">
            <?php
                if (isset($er_mail)){
            ?>
                <div><?= $er_mail ?></div>
            <?php   
                }
            ?>
            <div class="form-group">
                <label for="email"> Email</label>
                <input class="form-control" type="email" id="mail" placeholder="Adresse mail" name="mail" value="<?php if(isset($mail)){ echo $mail; }?>" required>
            </div>
            <?php
                if (isset($er_mdp)){
            ?>
                <div><?= $er_mdp ?></div>
            <?php   
                }
            ?>
            <div class="form-group">
                <label for="password"> Mot de passe</label>
                <input class="form-control" type="password" id="password" placeholder="Mot de passe" name="mdp" value="<?php if(isset($mdp)){ echo $mdp; }?>" required>
            </div>
            <button class="btn btn-primary" type="submit" name="connexion">Se connecter</button>
        </form>
            </div>
        </div>
    </div>
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </body>
</html>