   <?php
   // Permet de savoir s'il y a une session. 
   // C'est-a-dire si un utilisateur s'est connecte a notre site 
   session_start(); 
   // Fichier PHP contenant la connexion a notre BDD
   include('./connexionDB.php'); 
   ?>
   <!DOCTYPE html>
   <html>
   <head>
      <meta charset="utf-8"/>

      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

      <title>Accueil</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

   </head>

   <body>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="index.php">Accueil</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                  <?php
                     if(!isset($_SESSION['id'])){
                        // ... 
                     }else{
                     ?>
                        <li class="nav-item">
                              <a class="nav-link" href="profil.php">Mon profil</a>
                        </li>
                     <?php
                     } 
                  ?>
            </ul>
            <ul class="navbar-nav ml-md-auto">
                  <?php
                     if(!isset($_SESSION['id'])){
                     ?>
                        <li class="nav-item">
                              <a class="nav-link" href="inscription.php">Inscription</a>
                        </li>
                        <li class="nav-item">
                              <a class="nav-link" href="connexion.php">Connexion</a>
                        </li>                   
                     <?php
                     }else{
                     ?>
                        <li class="nav-item">
                              <a class="nav-link" href="deconnexion.php">Déconnexion</a>
                        </li>                     
            </ul>
         </div>
      </nav>

   <div class="container">
      <div class="row">   
         <div class="col-0 col-sm-0 col-md-2 col-lg-3"></div>
         <div class="col-12 col-sm-12 col-md-8 col-lg-6">
               <h1>Membriiens</h1>             
   <form method="post">
      <?php
         if (isset($er_prenom)){
      ?>
      <div><?= $er_prenom ?></div>
      <?php   
         }
      ?>
      <label for="prenom_search"> Rechercher un membre </label>
      <div class="form-group">
         
         <input type="text" id="prenom_search" placeholder="Un prénom" name="prenom" value="<?php if(isset($prenom)){ echo $prenom; }?>" required> 
      </div>
      <button class="btn btn-primary" type="submit" name="recherche">Envoyer</button>
      

   </form>
         </div>
      </div>
   </div>
   <?php 
      if(!empty($_POST)){
         extract($_POST);
         if (isset($_POST['recherche'])){
            $prenom  = htmlentities(trim($prenom));
         }
         $filter = ['users' => $prenom];
         $options = [];
         $query = new MongoDB\Driver\Query($filter, $options);
         $cursor = $mongodb->executeQuery('dbmongo.info_user', $query);
   ?>
         <h1> Liste des membres </h1>
   <?php
         foreach ($cursor as $document) {
            echo "<pre style=\"border: 1px solid #000;  overflow: auto; margin: 0.5em;\">";
            $objectToArray = (array)$document;
            echo 'Prenom : '.$objectToArray[0]."\n";
            echo 'Nom : '.$objectToArray[1]."\n";
            echo 'Mail : '.$objectToArray[2]."\n";
            echo 'Date de création du compte : '.$objectToArray[3]."\n";
            echo '</pre>';
         }
      } 

   ?>
   <?php

      } 
   ?>
            </ul><!-- Si nous ne rentrons pas dans le else (l'utilisateur est connecté) nous devons fermer ses balises car elles sont fermés dans le else (ligne 74) -->
         </div>
      </nav>
      <div class="container">
         <div class="row">   
         <div class="col-0 col-sm-0 col-md-2 col-lg-3"></div>
         <?php
         if(!isset($_SESSION['id'])){
         ?>
         <div class="col-12 col-sm-12 col-md-8 col-lg-6">
         <h1>Membriiens</h1>
         </div>
         </div>
      </div>
         <?php
         }
         ?>
   
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
   </body>

   </html>