<?php
    session_start();
    include('connexionDB.php'); // Fichier PHP contenant la connexion à votre BDD
 
    // S'il y a une session alors on ne retourne plus sur cette page
    if (isset($_SESSION['id'])){
        header('Location: index.php'); 
        exit;
    }
 
    // Si la variable "$_Post" contient des informations alors on les traitres
    if(!empty($_POST)){
        extract($_POST);
        $valid = true;
 
        // On se place sur le bon formulaire grâce au "name" de la balise "input"
        if (isset($_POST['inscription'])){
            $nom  = htmlentities(trim($nom)); // On récupère le nom
            $prenom = htmlentities(trim($prenom)); // on récupère le prénom
            $mail = htmlentities(strtolower(trim($mail))); // On récupère le mail
            $mdp = trim($mdp); // On récupère le mot de passe 
            $confmdp = trim($confmdp); //  On récupère la confirmation du mot de passe
 
            //  Vérification du nom
            if(empty($nom)){
                $valid = false;
                $er_nom = ("Le nom d' utilisateur ne peut pas être vide");
            }       
 
            //  Vérification du prénom
            if(empty($prenom)){
                $valid = false;
                $er_prenom = ("Le prenom d' utilisateur ne peut pas être vide");
            }       
 
            // Vérification du mail
            if(empty($mail)){
                $valid = false;
                $er_mail = "Le mail ne peut pas être vide";
 
                // On vérifit que le mail est dans le bon format
            }elseif(!preg_match("/^[a-z0-9\-_.]+@[a-z]+\.[a-z]{2,3}$/i", $mail)){
                $valid = false;
                $er_mail = "Le mail n'est pas valide";
 
            }else{
                // On vérifit que le mail est disponible
                $req_mail = $DB->query("SELECT mail FROM users WHERE mail = ?",
                    array($mail));
 
                $req_mail = $req_mail->fetch();
 
                if ($req_mail['mail'] <> ""){
                    $valid = false;
                    $er_mail = "Ce mail existe déjà";
                }
            }
 
            // Vérification du mot de passe
            if(empty($mdp)) {
                $valid = false;
                $er_mdp = "Le mot de passe ne peut pas être vide";
 
            }elseif($mdp != $confmdp){
                $valid = false;
                $er_mdp = "La confirmation du mot de passe ne correspond pas";
            }
 
            // Si toutes les conditions sont remplies alors on fait le traitement
            if($valid){
                $mdp = crypt($mdp, "$5$rounds=5000$usesomesillystringforsalt$");
                $date_creation_compte = date('Y-m-d H:i:s');
 
                // On insert nos données dans la table users
                $DB->insert("INSERT INTO users (nom, prenom, mail, mdp, date_creation_compte) VALUES 
                    (?, ?, ?, ?, ?)", 
                    array($nom, $prenom, $mail, $mdp, $date_creation_compte));
                $bulk->insert(['users' => $prenom,$prenom,$nom,$mail,$date_creation_compte]); // On prépare la requête d'insertion
                $mongodb->executeBulkWrite('dbmongo.info_user', $bulk); // on insère les données des utilisateurs dans la collection info_user de dbmongo
                header('Location: index.php');
                exit;
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Inscription</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php">Accueil</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
                <?php
                   if(!isset($_SESSION['id'])){

                   }else{
                   ?>
                      <li class="nav-item">
                            <a class="nav-link" href="profil.php">Mon profil</a>
                      </li>
                   <?php
                   } 
                ?>
          </ul>
          <ul class="navbar-nav ml-md-auto">
                <?php
                   if(!isset($_SESSION['id'])){
                   ?>
                      <li class="nav-item">
                            <a class="nav-link" href="inscription.php">Inscription</a>
                      </li>
                      <li class="nav-item">
                            <a class="nav-link" href="connexion.php">Connexion</a>
                      </li>
                   <?php
                   }else{
                   ?>
                      <li class="nav-item">
                            <a class="nav-link" href="deconnexion.php">Déconnexion</a>
                      </li>   
                    <?php
                   }
                ?>                  
          </ul>
       </div>
    </nav>
    
    <div class="container">

        <div class="row">   

            <div class="col-0 col-sm-0 col-md-2 col-lg-3"></div>

            <div class="col-12 col-sm-12 col-md-8 col-lg-6">
            <h1>Inscription</h1>
            <form method="post">
            <?php
                // S'il y a une erreur sur le nom alors on affiche
                if (isset($er_nom)){
                ?>
                    <div><?= $er_nom ?></div>
                <?php   
                }
            ?>
            <div class="form-group">
                <label for="name"> Nom</label>
                <input class="form-control" type="text" id="name" placeholder="Votre nom" name="nom" value="<?php if(isset($nom)){ echo $nom; }?>" required>
            </div>   
            <?php
                if (isset($er_prenom)){
                ?>
                    <div><?= $er_prenom ?></div>
                <?php   
                }
            ?>
            <div class="form-group">
                <label for="lastname"> Prenom</label>
                <input class="form-control" type="text" id ="lastname" placeholder="Votre prénom" name="prenom" value="<?php if(isset($prenom)){ echo $prenom; }?>" required>
            </div>   
            <?php
                if (isset($er_mail)){
                ?>
                    <div><?= $er_mail ?></div>
                <?php   
                }
            ?>
            <div class="form-group">
                <label for="email"> Email</label>
                <input class="form-control" type="email" id="email" placeholder="Adresse mail" name="mail" value="<?php if(isset($mail)){ echo $mail; }?>" required>
            </div>
            <?php
                if (isset($er_mdp)){
                ?>
                    <div><?= $er_mdp ?></div>
                <?php   
                }
            ?>
            <div class="form-group">
                <label for="password"> Mot de passe</label>
                <input class="form-control" type="password" id="password" placeholder="Mot de passe" name="mdp" value="<?php if(isset($mdp)){ echo $mdp; }?>" required>
            </div>
            <div class="form-group">
                <label for="password_verif"> Confirmation mot de passe</label>
                <input class="form-control" type="password" id="password_verif" placeholder="Confirmer le mot de passe" name="confmdp" required>
            </div>
            <button class="btn btn-primary" type="submit" name="inscription">S'inscrire</button>
            </div>
        </div>
    </div>
        </form>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </body>
</html>