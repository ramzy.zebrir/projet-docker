<?php
  session_start(); 
  include('connexionDB.php'); 
  // On vérifie qu'une session est active pour accéder à cette page
  if(!isset($_SESSION['id'])){ 
    header('Location: index.php'); 
    exit; 
  }

  // On récupère les informations de l'user connecté via son id 
  $afficher_profil = $DB->query("SELECT * 
    FROM users 
    WHERE id = ?", 
  array($_SESSION['id'])); 
  $afficher_profil = $afficher_profil->fetch(); 

?>

<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mon profil</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php">Accueil</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
  </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="profil.php">Mon profil</a>
                    </li>
        </ul>
        <ul class="navbar-nav ml-md-auto">       
                    <li class="nav-item">
                        <a class="nav-link" href="deconnexion.php">Déconnexion</a>
                    </li>                                     
        </ul>
    </div>
  </nav>
</br>
</br>
</br>
    <div class="container">
        <div class="row">   
            <div class="col-0 col-sm-0 col-md-2 col-lg-3"></div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-6">
 
    <ul class="list-group"> 
      <h3>Quelques informations sur vous :</h3>
      <li class="list-group-item">Votre id est : <?= $afficher_profil['id'] ?></li>
      <li class="list-group-item">Votre mail est : <?= $afficher_profil['mail'] ?></li>
      <li class="list-group-item">Votre prénom est : <?= $afficher_profil['prenom'] ?></li>
      <li class="list-group-item">Votre nom est : <?= $afficher_profil['nom'] ?></li>
      <li class="list-group-item">Votre compte a été crée le : <?= $afficher_profil['date_creation_compte'] ?></li>
    </ul>
           </div>
        </div>
    </div>
  </body>
</html>